FROM openjdk:8-jdk-alpine
ARG JAR_FILE=source/build/libs/*.jar
COPY ${JAR_FILE} app.jar
EXPOSE 4567
ENTRYPOINT ["java","-jar","/app.jar"]
